/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.buuuk.android.gallery;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.AbsListView.OnScrollListener;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//Need the following import to get access to the app resources, since this
//class is in a sub-package.
import com.buuuk.android.util.FileUtils;
import com.example.paracam.R;
import com.uklooneyjr.paracam.Globals;


@SuppressLint("UseSparseArrays")
public class ImageGridGallery extends Activity implements OnScrollListener {

    private GridView mGrid;
    /*
    private static final String DIRECTORY = "/sdcard/";
	private static final String DATA_DIRECTORY = "/sdcard/.ImageViewFlipper/";
	*/
	private static final String DATA_FILE = "/imagelist.dat";
	private List<String> mImageList;
	private int mGridImageSize = 320;
	
	private File getDir() {
		File sdDir = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		return new File(sdDir, Globals.PICTURE_DIRECTORY);
	}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.grid_gallery);
        mGrid = (GridView) findViewById(R.id.myGrid);
        mGrid.setOnScrollListener(this);
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	
    	loadApps(); // do this in onresume?
        
        File data_directory = getDir();
		if (!data_directory.exists()) {
			if (data_directory.mkdir()) {
				FileUtils savedata = new FileUtils();
				/*Toast toast = Toast.makeText(ImageGridGallery.this,
						"Please wait while we search your SD Card for images...", Toast.LENGTH_SHORT);
				toast.show();*/
				SystemClock.sleep(100);
				mImageList = FindFiles();
				savedata.saveArray(data_directory.getAbsolutePath() + DATA_FILE, mImageList);
				
			} else {
				mImageList = FindFiles();
			}

		}
		else {
			File data_file= new File(DATA_FILE);
			if (!data_file.exists()) {
				FileUtils savedata = new FileUtils();
				/*Toast toast = Toast.makeText(ImageGridGallery.this,
						"Please wait while we search your SD Card for images...", Toast.LENGTH_SHORT);
				toast.show();*/
				SystemClock.sleep(100);
				mImageList = FindFiles();
				savedata.saveArray(DATA_FILE, mImageList);
			} else {
				FileUtils readdata = new FileUtils();
				mImageList = readdata.loadArray(DATA_FILE);
			}
		}
		mAdapter = new AppsAdapter();
		 mGrid.setAdapter(mAdapter);
		 mThumbnails = new HashMap<Integer,SoftReference<ImageView>>();
         mThumbnailImages = new HashMap<Integer,SoftReference<Bitmap>>();
         
    	 ImageView galleryEmptyImage = (ImageView)findViewById(R.id.galleryEmpty_image);
         if (mImageList.size() == 0) {
        	 galleryEmptyImage.setVisibility(ImageView.VISIBLE);
         } else {
        	 galleryEmptyImage.setVisibility(ImageView.INVISIBLE);
         }
    }
    
    private List<String> FindFiles() {
		final List<String> tFileList = new ArrayList<String>();
		Resources resources = getResources();
		// array of valid image file extensions
		String[] imageTypes = resources.getStringArray(R.array.image);
		FilenameFilter[] filter = new FilenameFilter[imageTypes.length];

		int i = 0;
		for (final String type : imageTypes) {
			filter[i] = new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith("." + type);
				}
			};
			i++;
		}

		FileUtils fileUtils = new FileUtils();
		File[] allMatchingFiles = fileUtils.listFilesAsArray(
				getDir(), filter, 0);
		for (File f : allMatchingFiles) {
			tFileList.add(f.getAbsolutePath());
		}
		return tFileList;
	}

    @SuppressWarnings("unused")
	private List<ResolveInfo> mApps;

    private void loadApps() {
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        mApps = getPackageManager().queryIntentActivities(mainIntent, 0);
    }

    public AppsAdapter mAdapter;
    public class AppsAdapter extends BaseAdapter {
        @SuppressWarnings({ "rawtypes", "unchecked" })
		public AppsAdapter() {
        	map = new HashMap();
        }
        
        public Map<Integer,SoftReference<Bitmap>> map;
        public View getView(final int position, View convertView, ViewGroup parent) {
            ImageView i;

            if (convertView == null) {
                i = new ImageView(ImageGridGallery.this);
                i.setScaleType(ImageView.ScaleType.FIT_CENTER);
                i.setLayoutParams(new GridView.LayoutParams(mGridImageSize, mGridImageSize));
            } else {
                i = (ImageView) convertView;
            }
            
            if(!mBusy && mThumbnailImages.containsKey(position) 
        			&& mThumbnailImages.get(position).get()!=null) {
        		i.setImageBitmap(mThumbnailImages.get(position).get());
        	}
        	else  {
        		i.setImageBitmap(null);
        		if(!mBusy)loadThumbnail(i,position);
        	}
            
            i.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Toast.makeText(ImageGridGallery.this, "Opening Image...", Toast.LENGTH_LONG).show();

					// TODO Auto-generated method stub
					SharedPreferences indexPrefs = getSharedPreferences("currentIndex",
							MODE_PRIVATE);
					
					SharedPreferences.Editor indexEditor = indexPrefs.edit();
					indexEditor.putInt("currentIndex", position);
					indexEditor.commit();
					final Intent intent = new Intent(ImageGridGallery.this, ImageViewFlipper.class);
		            startActivity(intent);
		           
				}
			});
            
            
            return i;
        }


        public final int getCount() {
            return mImageList.size();
        }

        public final Object getItem(int position) {
            return mImageList.get(position);
        }

        public final long getItemId(int position) {
            return position;
        }
    }
    public void onScroll(AbsListView view, int firstVisibleItem,
    	    int visibleItemCount, int totalItemCount) {
    	
    }
    
    public boolean mBusy = false;
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        switch (scrollState) {
        case OnScrollListener.SCROLL_STATE_IDLE:
            mBusy = false;
            mAdapter.notifyDataSetChanged();
            break;
        case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
            mBusy = true;
            // mStatus.setText("Touch scroll");
            break;
        case OnScrollListener.SCROLL_STATE_FLING:
            mBusy = true;
            // mStatus.setText("Fling");
            break;
        }
    }
    
    private Map<Integer,SoftReference<ImageView>> mThumbnails;
    private Map<Integer,SoftReference<Bitmap>> mThumbnailImages;

    private void loadThumbnail( ImageView iv, int position ){
    	mThumbnails.put(position,new SoftReference<ImageView>(iv));
    	try{new LoadThumbnailTask().execute(position);}catch(Exception e){}
    }
    public void onThumbnailLoaded( int position, Bitmap bm, LoadThumbnailTask t ){
    	Bitmap tn = bm;
    	if( mThumbnails.get(position).get() != null && tn!=null)
    		mThumbnails.get(position).get().setImageBitmap(tn);
    	
    	t.cancel(true);
    }
    
    public class LoadThumbnailTask extends AsyncTask<Integer, Void, Bitmap>{
    	private int position;
		@Override
		protected Bitmap doInBackground(Integer... params) {
        	try{
				position = params[0];
				Bitmap bitmapOrg = BitmapFactory.decodeFile(mImageList.get(position));
	        
	        	int width = bitmapOrg.getWidth();
	        	int height = bitmapOrg.getHeight();
	     
	        	//new width / height
	        	int newWidth = mGridImageSize;
	        	int newHeight = mGridImageSize;
	
	        	// calculate the scale
	        	float scaleWidth = (float) newWidth / width;
	        	float scaleHeight = (float) newHeight/ (height * scaleWidth) ;
	        	// create a matrix for the manipulation
	        	Matrix matrix = new Matrix();
	
	        	// resize the bit map
	        	matrix.postScale(scaleWidth, scaleWidth);
	        	matrix.postScale(scaleHeight, scaleHeight);
	
	        	// recreate the new Bitmap and set it back
	        	Bitmap bm = Bitmap.createBitmap(bitmapOrg, 0, 0,width, height, matrix, true);
	            
	            mThumbnailImages.put(position, new SoftReference<Bitmap>(bm));
	            System.gc();
	            return bm;
        	}catch(Exception e){
        		
        	}
            
            
			return null;
		}
		protected void onPostExecute(Bitmap bm) {
	         
	         onThumbnailLoaded(position, bm, this);
	     }
    	
    }

}
