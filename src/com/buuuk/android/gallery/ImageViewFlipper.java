/**
 * This class is a modified version of alvinsj's AndroidImageGallery app
 * The original source code can be found on this GitHub page:
 * 	https://github.com/alvinsj/android-image-gallery
 * 
 * Changes made:
 * 	- Added a share button which uses an Intent chooser to share the currently 
 * 		selected image using another application on the device.
 * 	- Removed the DIRECTORY and DATA_DIRECTORY variables, now using 
 * 		getExternalStoragePublicDirectory() instead
 *  - Added a delete button for deleting images in the gallery
 *  - Added access to the SQLite database so entries in the database are deleted
 *  	when the image is deleted.
 * 
 */

package com.buuuk.android.gallery;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import com.buuuk.android.ui.touch.TouchActivity;
import com.buuuk.android.util.FileUtils;
import com.example.paracam.R;
import com.uklooneyjr.paracam.Globals;
import com.uklooneyjr.paracam.ParaCamDbAdapter;
import com.uklooneyjr.paracam.activities.MapActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
//import android.widget.Toast;
import android.widget.ViewFlipper;

public class ImageViewFlipper extends TouchActivity {

	private static final int EXIT = 0;
	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_MAX_OFF_PATH = 250;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;
	
	private static final String TAG = "ImageViewFilpper";
	private static final String DATA_FILE = "/imagelist.dat";
	private GestureDetector mGestureDetector;
	View.OnTouchListener mGestureListener;
	private Animation mSlideLeftIn;
	private Animation mSlideLeftOut;
	private Animation mSlideRightIn;
	private Animation mSlideRightOut;
	private ViewFlipper mViewFlipper;
	private int mCurrentView = 0;
	private List<String> mImageList;
	private int mCurrentIndex = 0;
	private int mMaxIndex = 0;
	@SuppressWarnings("unused")
	private ImageView mCurrentImageView = null;
	private Button mShareButton = null;
	private Button mDeleteButton = null;
	private Button mOpenMapButton = null;
	
	private ParaCamDbAdapter mDbHelper;

	private float mMinZoomScale = 1;

	FileOutputStream output = null;
	OutputStreamWriter writer = null;

	private File getDir() {
		File sdDir = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		return new File(sdDir, Globals.PICTURE_DIRECTORY);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mDbHelper = new ParaCamDbAdapter(this);
		mDbHelper.open();

		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.scrolling_gallery);
		ImageView iv = (ImageView) findViewById(R.id.zero);
		/*
		 * iv.setOnTouchListener(this);
		 * findViewById(R.id.one).setOnTouchListener(this);
		 * findViewById(R.id.two).setOnTouchListener(this);
		 */

		File data_directory = getDir();
		if (!data_directory.exists()) {
			if (data_directory.mkdir()) {
				FileUtils savedata = new FileUtils();
				/*Toast toast = Toast
						.makeText(
								ImageViewFlipper.this,
								"Please wait while we search your SD Card for images...",
								Toast.LENGTH_SHORT);
				toast.show();*/
				SystemClock.sleep(100);
				mImageList = FindFiles();
				savedata.saveArray(DATA_FILE, mImageList);

			} else {
				mImageList = FindFiles();
			}

		} else {
			File data_file = new File(DATA_FILE);
			if (!data_file.exists()) {
				FileUtils savedata = new FileUtils();
				/*Toast toast = Toast
						.makeText(
								ImageViewFlipper.this,
								"Please wait while we search your SD Card for images...",
								Toast.LENGTH_SHORT);
				toast.show();*/
				SystemClock.sleep(100);
				mImageList = FindFiles();
				savedata.saveArray(DATA_FILE, mImageList);
			} else {
				FileUtils readdata = new FileUtils();
				mImageList = readdata.loadArray(DATA_FILE);
			}
		}

		if (mImageList == null) {
			quit();
		}

		SharedPreferences indexPrefs = getSharedPreferences("currentIndex",
				MODE_PRIVATE);
		if (indexPrefs.contains("currentIndex")) {
			mCurrentIndex = indexPrefs.getInt("currentIndex", 0);
		}

		mMaxIndex = mImageList.size() - 1;

		Log.v("currentIndex", "Index: " + mCurrentIndex);

		mViewFlipper = (ViewFlipper) findViewById(R.id.flipper);

		mSlideLeftIn = AnimationUtils.loadAnimation(this, R.anim.slide_left_in);
		mSlideLeftOut = AnimationUtils
				.loadAnimation(this, R.anim.slide_left_out);
		mSlideRightIn = AnimationUtils
				.loadAnimation(this, R.anim.slide_right_in);
		mSlideRightOut = AnimationUtils.loadAnimation(this,
				R.anim.slide_right_out);

		mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(this,
				android.R.anim.fade_in));
		mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this,
				android.R.anim.fade_out));
		Drawable d = Drawable.createFromPath(mImageList.get(mCurrentIndex));
		iv.setImageDrawable(d);
		resetImage(iv, d);
		System.gc();

		mGestureDetector = new GestureDetector(new MyGestureDetector());
		mGestureListener = new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (mGestureDetector.onTouchEvent(event)) {
					return true;
				}
				return false;
			}
		};

		mShareButton = (Button) findViewById(R.id.btn_uploadImage);
		mShareButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String path = mImageList.get(mCurrentIndex);
				File file = new File(path);
				Uri imageUri = Uri.fromFile(file);

				Intent sharingIntent = new Intent(Intent.ACTION_SEND);
				sharingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				sharingIntent.putExtra(Intent.EXTRA_TEXT, "I found a ghost using ParaCam!");
				sharingIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
				sharingIntent.setType("image/jpg");

				startActivity(Intent.createChooser(sharingIntent,
						"Send image using.."));
			}
		});
		
		mDeleteButton = (Button)findViewById(R.id.btn_deleteButton);
		mDeleteButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String path = mImageList.get(mCurrentIndex);
				File file = new File(path);
				file.delete();
				
				if (--mMaxIndex < 0) {
					ImageViewFlipper.this.finish();
					return;
				} else {
					Log.d(TAG, "deleting image, " + (mMaxIndex + 1) + " left");
				}
				
				Cursor deletedFileEntry = mDbHelper.fetchGhostEntry(file.getName());
				if (deletedFileEntry.getCount() != 0) {
					int rowId = deletedFileEntry.getInt(deletedFileEntry.getColumnIndexOrThrow(ParaCamDbAdapter.KEY_ROWID));
					mDbHelper.deleteGhostEntry(rowId);
					Log.i(TAG, "deleted image from the Database");
				} else {
					Log.w(TAG, "deleted image did not have an associated entry in the Database");
				}
				
				int deletedFileIndex = mCurrentIndex;
				
				mViewFlipper.setInAnimation(mSlideLeftIn);
				mViewFlipper.setOutAnimation(mSlideLeftOut);

				if (mCurrentIndex >= mMaxIndex) {
					mCurrentIndex = 0;
				} else {
					mCurrentIndex = mCurrentIndex + 1;
				}
				ImageView iv;
				Drawable d = Drawable.createFromPath(mImageList
						.get(mCurrentIndex));

				if (mCurrentView == 0) {
					mCurrentView = 1;
					iv = (ImageView) findViewById(R.id.one);

					iv.setImageDrawable(d);

					System.gc();
				} else if (mCurrentView == 1) {
					mCurrentView = 2;
					iv = (ImageView) findViewById(R.id.two);

					iv.setImageDrawable(d);
					System.gc();
				} else {
					mCurrentView = 0;
					iv = (ImageView) findViewById(R.id.zero);

					iv.setImageDrawable(d);
					System.gc();
				}
				resetImage(iv, d);
				Log.v("ImageViewFlipper", "Current View: " + mCurrentView);
				mViewFlipper.showNext();
				
				mImageList.remove(deletedFileIndex);
				if (mCurrentIndex != 0) {
					mCurrentIndex--;
				}
				
				deletedFileEntry.close();
			}
		});
		
		mOpenMapButton = (Button)findViewById(R.id.btn_openMap);
		mOpenMapButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SharedPreferences indexPrefs = getSharedPreferences("currentImageRowId",
						MODE_PRIVATE);
				
				String path = mImageList.get(mCurrentIndex);
				File file = new File(path);
				
				int rowId = -1;
				Cursor currentGhost = mDbHelper.fetchGhostEntry(file.getName());
				if (currentGhost.getCount() != 0) {
					rowId = currentGhost.getInt(currentGhost.getColumnIndexOrThrow(ParaCamDbAdapter.KEY_ROWID));
				}
				
				SharedPreferences.Editor indexEditor = indexPrefs.edit();
				indexEditor.putInt("currentImageRowId", rowId);
				indexEditor.commit();
				
				currentGhost.close();
				
				Intent intent = new Intent(ImageViewFlipper.this, MapActivity.class);
		        startActivity(intent);
			}
			
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);

		int NONE = Menu.NONE;
		menu.add(NONE, EXIT, NONE, "Exit");
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case EXIT:
			quit();
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	protected void onPause() {
		super.onPause();

		SharedPreferences indexPrefs = getSharedPreferences("currentIndex",
				MODE_PRIVATE);

		SharedPreferences.Editor indexEditor = indexPrefs.edit();
		indexEditor.putInt("currentIndex", mCurrentIndex);
		indexEditor.commit();
	}

	protected void onResume() {
		super.onResume();
		SharedPreferences indexPrefs = getSharedPreferences("currentIndex",
				MODE_PRIVATE);
		if (indexPrefs.contains("currentIndex")) {
			mCurrentIndex = indexPrefs.getInt("currentIndex", 0);
		}
	}

	private List<String> FindFiles() {
		final List<String> tFileList = new ArrayList<String>();
		Resources resources = getResources();
		// array of valid image file extensions
		String[] imageTypes = resources.getStringArray(R.array.image);
		FilenameFilter[] filter = new FilenameFilter[imageTypes.length];

		int i = 0;
		for (final String type : imageTypes) {
			filter[i] = new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith("." + type);
				}
			};
			i++;
		}

		FileUtils fileUtils = new FileUtils();
		File[] allMatchingFiles = fileUtils.listFilesAsArray(getDir(), filter,
				0);
		for (File f : allMatchingFiles) {
			tFileList.add(f.getAbsolutePath());
		}
		return tFileList;
	}

	class MyGestureDetector extends SimpleOnGestureListener {
		@Override
		public boolean onDoubleTap(final MotionEvent e) {

			ImageView view = (ImageView) findViewById(R.id.zero);
			switch (mCurrentView) {
			case 0:
				view = (ImageView) findViewById(R.id.zero);
				break;
			case 1:
				view = (ImageView) findViewById(R.id.one);
				break;
			case 2:
				view = (ImageView) findViewById(R.id.two);
				break;
			}

			resetImage(view, view.getDrawable());
			return true;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			try {
				if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
					return false;
				// right to left swipe
				if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					mViewFlipper.setInAnimation(mSlideLeftIn);
					mViewFlipper.setOutAnimation(mSlideLeftOut);

					if (mCurrentIndex == mMaxIndex) {
						mCurrentIndex = 0;
					} else {
						mCurrentIndex = mCurrentIndex + 1;
					}
					ImageView iv;
					Drawable d = Drawable.createFromPath(mImageList
							.get(mCurrentIndex));

					if (mCurrentView == 0) {
						mCurrentView = 1;
						iv = (ImageView) findViewById(R.id.one);

						iv.setImageDrawable(d);

						System.gc();
					} else if (mCurrentView == 1) {
						mCurrentView = 2;
						iv = (ImageView) findViewById(R.id.two);

						iv.setImageDrawable(d);
						System.gc();
					} else {
						mCurrentView = 0;
						iv = (ImageView) findViewById(R.id.zero);

						iv.setImageDrawable(d);
						System.gc();
					}
					resetImage(iv, d);
					Log.v("ImageViewFlipper", "Current View: " + mCurrentView);
					mViewFlipper.showNext();

					return true;
				} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					mViewFlipper.setInAnimation(mSlideRightIn);
					mViewFlipper.setOutAnimation(mSlideRightOut);

					if (mCurrentIndex == 0) {
						mCurrentIndex = mMaxIndex;
					} else {
						mCurrentIndex = mCurrentIndex - 1;
					}
					ImageView iv;
					Drawable d = Drawable.createFromPath(mImageList
							.get(mCurrentIndex));
					if (mCurrentView == 0) {
						mCurrentView = 2;
						iv = (ImageView) findViewById(R.id.two);
						iv.setImageDrawable(d);
						System.gc();
					} else if (mCurrentView == 2) {
						mCurrentView = 1;
						iv = (ImageView) findViewById(R.id.one);
						iv.setImageDrawable(d);
						System.gc();
					} else {
						mCurrentView = 0;
						iv = (ImageView) findViewById(R.id.zero);
						iv.setImageDrawable(d);
						System.gc();
					}
					resetImage(iv, d);
					Log.v("ImageViewFlipper", "Current View: " + mCurrentView);
					mViewFlipper.showPrevious();
					return true;
				}
			} catch (Exception e) {
				// nothing
			}
			return false;
		}

	}

	@SuppressWarnings("deprecation")
	@Override
	public void resetImage(ImageView iv, Drawable draw) {
		Display display = ((WindowManager) getSystemService(WINDOW_SERVICE))
				.getDefaultDisplay();
		int rotation = display.getRotation();

		int orientation = 0;
		if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180)
			orientation = 0;
		else
			orientation = 1;

		matrix = new Matrix();
		matrix.setTranslate(1f, 1f);
		float scale = 1;

		mMinZoomScale = 1;
		if (orientation == 0
		// && (float)draw.getIntrinsicWidth() >
		// (float)getWindowManager().getDefaultDisplay().getWidth()
		) {

			scale = (float) getWindowManager().getDefaultDisplay().getWidth()
					/ (float) draw.getIntrinsicWidth();
			mMinZoomScale = scale;
			matrix.postScale(scale, scale);

			iv.setImageMatrix(matrix);
		} else if (orientation == 1
		// && (float)draw.getIntrinsicHeight() >
		// (float)getWindowManager().getDefaultDisplay().getHeight()
		) {
			scale = (float) getWindowManager().getDefaultDisplay().getHeight()
					/ (float) draw.getIntrinsicHeight();
			mMinZoomScale = scale;
			matrix.postScale(scale, scale);

			iv.setImageMatrix(matrix);
		}

		float transX = (float) getWindowManager().getDefaultDisplay()
				.getWidth()
				/ 2
				- (float) (draw.getIntrinsicWidth() * scale)
				/ 2;

		float transY = (float) getWindowManager().getDefaultDisplay()
				.getHeight()
				/ 2
				- (float) (draw.getIntrinsicHeight() * scale)
				/ 2

		;
		matrix.postTranslate(transX, transY);
		iv.setImageMatrix(matrix);
	}

	@Override
	public float getMinZoomScale() {
		return mMinZoomScale;
	}

	@Override
	public boolean onTouchEvent(MotionEvent rawEvent) {
		if (mGestureDetector.onTouchEvent(rawEvent))
			return true;

		ImageView view = (ImageView) findViewById(R.id.zero);
		switch (mCurrentView) {
		case 0:
			view = (ImageView) findViewById(R.id.zero);
			break;
		case 1:
			view = (ImageView) findViewById(R.id.one);
			break;
		case 2:
			view = (ImageView) findViewById(R.id.two);
			break;
		}
		onTouchEvented(view, rawEvent);

		return true;
	}

	public void quit() {
		SharedPreferences indexPrefs = getSharedPreferences("currentIndex",
				MODE_PRIVATE);

		SharedPreferences.Editor indexEditor = indexPrefs.edit();
		indexEditor.putInt("currentIndex", 0);
		indexEditor.commit();

		File settings = new File(DATA_FILE);
		settings.delete();
		finish();
		int pid = android.os.Process.myPid();
		android.os.Process.killProcess(pid);
		System.exit(0);
	}

}