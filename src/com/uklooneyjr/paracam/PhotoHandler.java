package com.uklooneyjr.paracam;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import com.uklooneyjr.paracam.activities.GameActivity;
import com.uklooneyjr.paracam.views.CameraPreview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

@SuppressLint("SimpleDateFormat")
public class PhotoHandler implements PictureCallback {
	/**
	 * Handles the callback from the camera in GameActivity
	 */

	private static final String TAG = "PhotoHandler";
	
	private static final float IMAGE_SCALE_FACTOR = 0.5f;

	private final Context mContext;
	
	private Thread mImageProcessingThread;
	
	private SharedPreferences mPrefs;
	private CameraPreview mPreview;
	private Activity mActivity;
	private int mGhostType;
	private byte[] mData;
	private String mFilename;

	public PhotoHandler(GameActivity activity, Context context, CameraPreview preview, int ghostType, String filename) {
		mActivity = activity;
		mContext = context;
		mPreview = preview;
		mGhostType = ghostType;
		mFilename = filename;
		
		mPrefs = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		
		// Do the image processing in a seperate thread
		mImageProcessingThread = new Thread(new Runnable() {

			@Override
			public void run() {
				File pictureFileDir = getDir();
				
				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						String name = mPrefs.getString("prefName", "Anon");
						Toast.makeText(mContext, "One moment " + name + ", I'll just remove the static.",
								Toast.LENGTH_LONG).show();
					}
				});

				
				mPreview.startPreview();

				if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {

					Log.d(TAG, "Can't create directory to save image.");
					mActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(mContext, "Can't create directory to save image.",
									Toast.LENGTH_LONG).show();
						}
					});

					return;

				}

				String filepath = pictureFileDir.getPath() + File.separator + mFilename;

				File pictureFile = new File(filepath);
				
				Log.d(TAG, "Saving photo to " + pictureFileDir.getAbsolutePath());
				
				Bitmap bitmap = addGhostToImage();
				if (Globals.DEBUG) Log.d(TAG + "mod", "creating blob");
				// recompress the bitmap to a jpg
				ByteArrayOutputStream blob = new ByteArrayOutputStream();
				bitmap.compress(CompressFormat.JPEG, 40, blob);
				if (Globals.DEBUG) Log.d(TAG + "mod", "image compressed, writing file");
				byte[] bitmapdata = blob.toByteArray();

				try {
					// save the bitmap
					FileOutputStream fos = new FileOutputStream(pictureFile);
					fos.write(bitmapdata);
					fos.close();
					mActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							String name = mPrefs.getString("prefName", "Anon");
							Toast.makeText(mContext, "Good job " + name + ", you've got a picture of it! Check it out in the gallery.",
									Toast.LENGTH_LONG).show();
						}
					});

				} catch (Exception error) {
					Log.d(TAG, "File" + filepath + "not saved: "
							+ error.getMessage());
					mActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(mContext, "Image could not be saved.",
									Toast.LENGTH_LONG).show();
						}
					});

				}
			}
			
			private Bitmap addGhostToImage() {
				
				if (Globals.DEBUG) Log.d(TAG + "-mod", "addGhostToImage()");
				Bitmap src = BitmapFactory.decodeByteArray(mData , 0, mData.length);
				if (Globals.DEBUG) Log.d(TAG + "-mod", "byte array decoded");
				int w = src.getWidth();
			    int h = src.getHeight();
			    // swap the width and height since we will be rotating the image
			    Bitmap result = Bitmap.createBitmap((int) (h * IMAGE_SCALE_FACTOR),(int) (w * IMAGE_SCALE_FACTOR), src.getConfig());
				Bitmap ghostGraphic = Ghost.loadGraphicByType(mContext.getResources(), mGhostType, false);
				
				Paint paint = new Paint();
				paint.setAlpha(Ghost.loadAlphaByDifficulty(1));
				paint.setAntiAlias(true);
				paint.setFilterBitmap(true);
				paint.setDither(true);
				
				Canvas canvas = new Canvas(result);
				
				// Since the camera takes it's picture at an orientation of 90 degrees, we must correct it
				canvas.save();
				
				canvas.rotate(90);
				canvas.scale(IMAGE_SCALE_FACTOR, IMAGE_SCALE_FACTOR);
				canvas.drawBitmap(src, 0, -h, null);
				if (Globals.DEBUG) Log.d(TAG + "-mod", "src drawn");
				
				canvas.restore();
				
				canvas.scale(20.0f * IMAGE_SCALE_FACTOR, 20.0f * IMAGE_SCALE_FACTOR);
				canvas.drawBitmap(ghostGraphic, 5, 30, paint);		
				Log.d("TAG", ghostGraphic.toString() + " ");		
				
				if (Globals.DEBUG) Log.d(TAG + "-mod", "ghost drawn");
				
				return result;
				
			}
			
		});
	}

	@Override
	public void onPictureTaken(byte[] data, Camera camera) {
		
		if (mImageProcessingThread.isAlive()) return;
		
		mData = data;
		
		mImageProcessingThread.start();
	}

	private File getDir() {
		File sdDir = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		return new File(sdDir, Globals.PICTURE_DIRECTORY);
	}
} 