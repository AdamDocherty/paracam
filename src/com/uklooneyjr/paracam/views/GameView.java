package com.uklooneyjr.paracam.views;

import com.uklooneyjr.paracam.EnergyMeter;
import com.uklooneyjr.paracam.Ghost;
import com.uklooneyjr.paracam.Globals;
import com.uklooneyjr.paracam.Static;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class GameView extends View {
	
	private static final String TAG = "GameView";
	
	private static boolean sDebugText = false;

	private Paint mPaintDebug = new Paint();
	
	// Azimut, Pitch and Roll values, only used to print out for debugging purposes
	private float mAzimut, mPitch, mRoll;

	// References to the game objects for drawing
	private Ghost mGhost;
	private Static mStatic;
	private EnergyMeter[] mEnergyMeters;
	
	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		if (Globals.DEBUG) Log.e(TAG, "GameView()");
		
		sDebugText = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("prefDebugVaules", false);
		
		//setFocusable(true); // make sure we get key events
		
		mPaintDebug = new Paint();
		mPaintDebug.setAntiAlias(true);
		mPaintDebug.setDither(false);
		mPaintDebug.setColor(0xFFFF0000);
		mPaintDebug.setStyle(Paint.Style.STROKE);
		mPaintDebug.setTextSize(24);

	}
	
	public void setGhost(Ghost ghost) {
		mGhost = ghost;
	}
	
	public void setStatic(Static aStatic) {
		mStatic = aStatic;
	}
	
	public void setEnergyMeters(EnergyMeter[] energyMeters) {
		mEnergyMeters = energyMeters;
	}
	
	public void setOrientation(float orientation[]) {
		mAzimut = orientation[0]; 
		mPitch  = orientation[1];
		mRoll   = orientation[2];
	}
	
	@Override
	protected void onDraw(Canvas canvas)
	{		
		//if (Globals.DEBUG) Log.e(TAG, "onDraw()");
		
		mStatic.draw(canvas);
		mGhost.draw(canvas);
		for (int i = 0; i < mEnergyMeters.length; ++i) {
			mEnergyMeters[i].draw(canvas);
		}
		
		if (sDebugText) {
			canvas.translate(200.f, 200.f);
			canvas.scale(3.0f, 3.0f);		
			
			canvas.drawText("Azimut = " + -mAzimut*360/(2*3.14159f), -40, 0,  mPaintDebug);
			canvas.drawText("Pitch  = " + (float) Math.toDegrees(mPitch) , -40, 25, mPaintDebug);
			canvas.drawText("Roll   = " + (float) Math.toDegrees(mRoll)  , -40, 50, mPaintDebug);
			canvas.drawText("Ghost X = "  + mGhost.getX() , -40, 75, mPaintDebug);
			canvas.drawText("Ghost Y = "  + mGhost.getY() , -40, 100, mPaintDebug);
			canvas.drawText("Relative X = "  + mGhost.getScreenX() , -40, 125, mPaintDebug);
			canvas.drawText("Relative Y = "  + mGhost.getScreenY() , -40, 150, mPaintDebug);
		}
	}
}
