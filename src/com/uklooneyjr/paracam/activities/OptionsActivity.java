package com.uklooneyjr.paracam.activities;

import com.example.paracam.R;

import android.os.Bundle;
import android.preference.PreferenceActivity;

@SuppressWarnings("deprecation")
public class OptionsActivity extends PreferenceActivity {
 

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 
        addPreferencesFromResource(R.xml.settings);
        
        /*ListPreference languages = (ListPreference) findPreference("prefLanguage");
        if(languages.getValue()==null) {
            // to ensure we don't get a null value
            // set first value by default
        	languages.setValueIndex(0);
        }
        languages.setSummary(languages.getValue().toString());
        languages.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(newValue.toString());  
                return true;
            }
        }); */
    }
}