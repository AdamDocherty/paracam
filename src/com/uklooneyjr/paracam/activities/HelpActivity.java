package com.uklooneyjr.paracam.activities;

import com.example.paracam.R;
import com.uklooneyjr.paracam.Globals;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class HelpActivity extends Activity {
	
	private static final String TAG = "HelpActivity";
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (Globals.DEBUG) Log.e(TAG, "onCreate()");
        
        setContentView(R.layout.help);
	}
}
