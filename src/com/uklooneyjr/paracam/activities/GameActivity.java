package com.uklooneyjr.paracam.activities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import com.example.paracam.R;
import com.uklooneyjr.paracam.EnergyMeter;
import com.uklooneyjr.paracam.Ghost;
import com.uklooneyjr.paracam.Globals;
import com.uklooneyjr.paracam.ParaCamDbAdapter;
import com.uklooneyjr.paracam.PhotoHandler;
import com.uklooneyjr.paracam.Static;
import com.uklooneyjr.paracam.views.CameraPreview;
import com.uklooneyjr.paracam.views.GameView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

@SuppressLint("SimpleDateFormat")
public class GameActivity extends Activity implements OnKeyListener, SensorEventListener {

	private static final String TAG = "GameActivity";
	
//	private static final long MINIMUM_DISTANCECHANGE_FOR_UPDATE = 1; // in Meters
//    private static final long MINIMUM_TIME_BETWEEN_UPDATE = 1000; // in Milliseconds
    private static final long POINT_RADIUS = 100; // in Meters
    private static final long PROX_ALERT_EXPIRATION = -1; // It will never expire
    private static final String PROX_ALERT_INTENT = "com.androidmyway.demo.ProximityAlert";

	private CameraPreview mPreview;			// camera preview view
	private Camera mCamera;					// the first rear facing camera
	
	private SharedPreferences mSharedPrefs;	// the private preferences for the app
	
	private GameView mGameView;				// view for drawing the game objects
	
	private Handler mHandler;				// used to call the game update loop
	private GameUpdate mGameUpdate;			// the main game update loop, called by the handler every 100ms
	
	private SensorManager mSensorManager;
	private Sensor mGravitySensor;
	private Sensor mMagnetometer;
	
	// raw inputs from Android sensors
	private float mNormGravity;           	// length of raw gravity vector received in onSensorChanged().  should be about 10
    private float[] mNormGravityVector;   	// Normalised gravity vector, which points straight up into space
    private float mNormMagField;          	// length of raw magnetic field vector received in onSensorChanged(). 
    private float[] mNormMagFieldValues;   	// Normalised magnetic field vector

    // values calculated once gravity and magnetic field vectors are available
    private float[] mNormEastVector;       	// normalised cross product of raw gravity vector with magnetic field values, points east
    private float[] mNormNorthVector;      	// Normalised vector pointing to magnetic north
    private float mAzimuthRadians;        	// angle of the device from magnetic north
    private float mPitchRadians;          	// tilt angle of the device from the horizontal.  m_pitch_radians = 0 if the device if flat, m_pitch_radians = Math.PI/2 means the device is upright.
    private float mRollRadians;
	
	private static final int HISTORICAL_AZIMUTH_RADIANS = 20; // number of historical azimuth values stored
	private int mHistoricalAzimuthRadiansIndex = -1; // next index to be filled in in the HistoricalAzimuthRadians array
	private float[] mHistoricalAzimuthRadians;  // previous values of the azimuth, used for a moving average to smooth out movement
	
    private SoundPool mSoundPool;				// sound pool to store the camera shutter sound
	
    private boolean mHauntGhostFound;
    
	// Game objects
	private static final int NUMBER_ENERGY_METERS = 3;
	
	private Ghost mGhost;						// represents the ghost
	private Static mStatic;						// adds static to the screen the closer the camera is to the ghost
	private EnergyMeter[] mEnergyMeters;		// Energy meters that flicker when the ghost is in view
	
	private LocationManager mLocationManager;   // The location manager for getting the current location when a picture is taken
	private Location mCurrentBestLocation = null; // The current best location
	
	private ProximityIntentReceiver mProximityIntentReceiver = null;
	
	private ParaCamDbAdapter mDbHelper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (Globals.DEBUG) Log.e(TAG, "onCreate()");
		setContentView(R.layout.game);
		
		mSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(GameActivity.this);
		mDbHelper = new ParaCamDbAdapter(this);
		mDbHelper.open();
		
		mCamera = getCameraInstance();
		
		mHandler = new Handler();
		
		mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);
        
        mGameView = (GameView)findViewById(R.id.game_view);
        
        // get the Sensors from the sensor manager
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mGravitySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);    
        
        mNormEastVector = new float[3];
        mNormNorthVector = new float[3];        
        mNormGravityVector = new float[3];
        mNormMagFieldValues = new float[3];
        mHistoricalAzimuthRadians = new float[HISTORICAL_AZIMUTH_RADIANS];
        
        mGameUpdate = new GameUpdate(mGameView, mHandler);
        
        mGhost = new Ghost(getResources(), mSharedPrefs);
        mStatic = new Static(getResources());
        mEnergyMeters = new EnergyMeter[NUMBER_ENERGY_METERS];
        
        // get the screen density for positioning the energy meters so they appear in the same location for every device
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        float density = dm.density;
        
        for (int i = 0; i < mEnergyMeters.length; ++i) {
        	mEnergyMeters[i] = new EnergyMeter(density * 50, (density * 525) + (i*25));
        }
        
        // set up the sound pool that the camera snapshot will be played from
        mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        
        // pass the game objects into the view
        mGameView.setGhost(mGhost);
        mGameView.setStatic(mStatic);
        mGameView.setEnergyMeters(mEnergyMeters);
        
        mGameView.setOnLongClickListener(new OnLongClickListener() {
        	// load the camera shutter sound into the sound pool and store it's handle
        	int cameraShutterSound = mSoundPool.load(GameActivity.this, R.raw.camera_auto_focus, 1);
        	
			@Override
			public boolean onLongClick(View v) {
				
				if (!mHauntGhostFound) {
					return false;
				}
				
				boolean shutterSoundEnabled = mSharedPrefs.getBoolean("prefSnapshotSound", true);
				
				if (ghostInView()) {
					if (shutterSoundEnabled) {
						mSoundPool.play(cameraShutterSound, 1.0f, 1.0f, 0, 0, 1.0f);
					}
					
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
					String date = dateFormat.format(new Date());
					String filename = "ParaCam_" + date + ".jpg";
					
					takePicture(new PhotoHandler(GameActivity.this, getApplicationContext(), mPreview, mGhost.getGhostType(), filename));
					mCurrentBestLocation = getLastBestLocation();
					
					String username = mSharedPrefs.getString("prefName", "Anon");
					
					mDbHelper.createGhostEntry(filename, mGhost.getGhostType(), 
							(float)mCurrentBestLocation.getLongitude(), (float)mCurrentBestLocation.getLatitude(), 
							DateFormat.getDateTimeInstance().format(new Date()), username);
					
					SharedPreferences privatePreferences = getSharedPreferences("currentIndex",
							Context.MODE_PRIVATE);
					
					boolean hauntOn = privatePreferences.getBoolean("hauntTargetSearching", false);
					
					if (hauntOn) {
						SharedPreferences.Editor indexEditor = privatePreferences.edit();
				    	indexEditor.putBoolean("hauntTargetSearching", false);
						indexEditor.commit();
					}
				}
				return false;
			}
        	
        });
        
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
        // make sure the screen stays on even when the user is not touching the screen
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		// register sensor listeners
	    mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_UI);
	    mSensorManager.registerListener(this, mGravitySensor, SensorManager.SENSOR_DELAY_UI);
		
	    // start the game loop
		mHandler.removeCallbacks(mGameUpdate);
		mHandler.postDelayed(mGameUpdate, 33); // 33 ms resulting in 30 frames per second
		mGhost.resume();
		
		if (mSharedPrefs.getBoolean("prefHauntModeEnabled", true)) {
			addProximityAlert();
		} else {
			mHauntGhostFound = true;
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		mHandler.removeCallbacks(mGameUpdate);
		mGhost.pause();
		
		mSensorManager.unregisterListener(this);
		
		if (null != mCamera) {
			mCamera.setPreviewCallback(null);
			mPreview.getHolder().removeCallback(mPreview);
			
			mCamera.release();
			mCamera = null;
		}
		
		if (null != mProximityIntentReceiver) {
			unregisterReceiver(mProximityIntentReceiver);
			mProximityIntentReceiver = null;
		}
	}

	public Camera getCameraInstance(){
	    Camera c = null;
	    try {
	        c = Camera.open(); // attempt to get a Camera instance
	    }
	    catch (Exception e) {
	    	// Throw an alert dialogue if the camera cannot be accessed
	    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(R.string.camera_not_found_dialog_message)
				   .setTitle(R.string.camera_not_found_dialog_title);
			// Return to the title screen.
			AlertDialog dialog = builder.create();
			dialog.show();
			dialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialogInterface) {
					startActivity(new Intent(GameActivity.this, MainMenuActivity.class));
				}
			});
	    }
	    return c; // returns null if camera is unavailable
	}
	
	private void takePicture(PhotoHandler photoHandler) {		
		mCamera.takePicture(null, null,
		        photoHandler);
	}

	@Override
	public boolean onKey(View view, int keyCode, KeyEvent event) {
		if ((event.getAction() == KeyEvent.ACTION_DOWN) && 
				(keyCode == KeyEvent.KEYCODE_BACK))
		{
			startActivity(new Intent(this, MainMenuActivity.class));
			return true;
		}
		
		return false;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		/*
		 // This code is not as accurate as the below implementation
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
			mGravity = event.values;
		if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
			mGeomagnetic = event.values;
		if (mGravity != null && mGeomagnetic != null) {
			float R[] = new float[16];
			float I[] = new float[16];
			boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
			if (success) {
				float orientation[] = new float[3];
				SensorManager.getOrientation(R, orientation);
				mGameView.setOrientation(orientation);
	      }
	    } */
		
		int SensorType = event.sensor.getType();
        switch(SensorType) {
            case Sensor.TYPE_GRAVITY:
                if (mNormGravityVector == null) mNormGravityVector = new float[3];
                System.arraycopy(event.values, 0, mNormGravityVector, 0, mNormGravityVector.length);                   
                mNormGravity = (float)Math.sqrt(mNormGravityVector[0]*mNormGravityVector[0] + mNormGravityVector[1]*mNormGravityVector[1] + mNormGravityVector[2]*mNormGravityVector[2]);
                for(int i=0; i < mNormGravityVector.length; i++) mNormGravityVector[i] /= mNormGravity;
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                if (mNormMagFieldValues == null) mNormMagFieldValues = new float[3];
                System.arraycopy(event.values, 0, mNormMagFieldValues, 0, mNormMagFieldValues.length);
                mNormMagField = (float)Math.sqrt(mNormMagFieldValues[0]*mNormMagFieldValues[0] + mNormMagFieldValues[1]*mNormMagFieldValues[1] + mNormMagFieldValues[2]*mNormMagFieldValues[2]);
                for(int i=0; i < mNormMagFieldValues.length; i++) mNormMagFieldValues[i] /= mNormMagField;  
                break;
        }
        if (mNormGravityVector != null && mNormMagFieldValues != null) {
            // first calculate the horizontal vector that points due east
            float eastX = mNormMagFieldValues[1]*mNormGravityVector[2] - mNormMagFieldValues[2]*mNormGravityVector[1];
            float eastY = mNormMagFieldValues[2]*mNormGravityVector[0] - mNormMagFieldValues[0]*mNormGravityVector[2];
            float eastZ = mNormMagFieldValues[0]*mNormGravityVector[1] - mNormMagFieldValues[1]*mNormGravityVector[0];
            float norm_East = (float)Math.sqrt(eastX * eastX + eastY * eastY + eastZ * eastZ);
            if (mNormGravity * mNormMagField * norm_East < 0.1f) {  // Typical values are  > 100.
                
            } else {
                mNormEastVector[0] = eastX / norm_East; mNormEastVector[1] = eastY / norm_East; mNormEastVector[2] = eastZ / norm_East;

                // next calculate the horizontal vector that points due north                   
                float magDotGrav = (mNormGravityVector[0] *mNormMagFieldValues[0] + mNormGravityVector[1]*mNormMagFieldValues[1] + mNormGravityVector[2]*mNormMagFieldValues[2]);
                float northX = mNormMagFieldValues[0] - mNormGravityVector[0] * magDotGrav;
                float northY = mNormMagFieldValues[1] - mNormGravityVector[1] * magDotGrav;
                float northZ = mNormMagFieldValues[2] - mNormGravityVector[2] * magDotGrav;
                float normNorth = (float)Math.sqrt(northX * northX + northY * northY + northZ * northZ);
                mNormNorthVector[0] = northX / normNorth; mNormNorthVector[1] = northY / normNorth; mNormNorthVector[2] = northZ / normNorth;

                // take account of screen rotation away from its natural rotation
                // since the current orientation is locked, this should always be the same value.
                int rotation = getWindowManager().getDefaultDisplay().getRotation();
                float screen_adjustment = 0;
                switch(rotation) {
                    case Surface.ROTATION_0:   screen_adjustment =          0;         break;
                    case Surface.ROTATION_90:  screen_adjustment =   (float)Math.PI/2; break;
                    case Surface.ROTATION_180: screen_adjustment =   (float)Math.PI;   break;
                    case Surface.ROTATION_270: screen_adjustment = 3*(float)Math.PI/2; break;
                }
                // calculate all the required angles from the rotation matrix
                /** see http://math.stackexchange.com/questions/381649/whats-the-best-3d-angular-co-ordinate-system-for-working-with-smartfone-apps */
                float sin = mNormEastVector[1] -  mNormNorthVector[0], cos = mNormEastVector[0] +  mNormNorthVector[1];
                mAzimuthRadians = (float) (sin != 0 && cos != 0 ? Math.atan2(sin, cos) : 0);
                mPitchRadians = (float) Math.acos(mNormGravityVector[2]);
                sin = -mNormEastVector[1] -  mNormNorthVector[0]; cos = mNormEastVector[0] -  mNormNorthVector[1];
                //float aximuthPlusTwoPitchAxisRadians = (float)(sin != 0 && cos != 0 ? Math.atan2(sin, cos) : 0);
                mAzimuthRadians += screen_adjustment;
                mRollRadians = (float) Math.atan2(-mNormGravityVector[1], mNormGravityVector[0]);
                
                appendHistoricalAzimuth(mAzimuthRadians);
                
                mGameView.setOrientation(getOrientation());
            }
        }
	}
	
	/**
	 * Returns the azimuth, pitch and roll in a float array. All values are in radians.
	 * 
	 * @return [0] Azimuth, [1] Pitch, [2] Roll
	 */
	public float[] getOrientation() {
		float orientation[] = new float[] { getAverageAzimuthRadians(), mPitchRadians, mRollRadians };
		
		return orientation;
	}
	
	private void appendHistoricalAzimuth(float azimuth) {
		if (mHistoricalAzimuthRadiansIndex == -1) {
			for (int i = 0; i < HISTORICAL_AZIMUTH_RADIANS; ++i) {
				mHistoricalAzimuthRadians[i] = azimuth;
				mHistoricalAzimuthRadiansIndex = 0;
			}
		} else {
			mHistoricalAzimuthRadians[mHistoricalAzimuthRadiansIndex] = azimuth;
			if (++mHistoricalAzimuthRadiansIndex >= HISTORICAL_AZIMUTH_RADIANS) {
				mHistoricalAzimuthRadiansIndex = 0;
			}
		}
	}
	
	private float getAverageAzimuthRadians() {
		float avg = 0.0f;
		for (int i = 0; i < HISTORICAL_AZIMUTH_RADIANS; ++i) {
			avg += mHistoricalAzimuthRadians[i];
		}
		avg /= HISTORICAL_AZIMUTH_RADIANS;
		
		return avg;
	}
	
	private Location getLastBestLocation() {
	    Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	    Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

	    long GPSLocationTime = 0;
	    if (null != locationGPS) { GPSLocationTime = locationGPS.getTime(); }

	    long NetLocationTime = 0;
	    if (null != locationNet) {
	        NetLocationTime = locationNet.getTime();
	    }

	    if ( 0 < GPSLocationTime - NetLocationTime ) {
	        return locationGPS;
	    }
	    else {
	        return locationNet;
	    }
	}
	
	private boolean ghostInView() {
		float x = mGhost.getScreenX();
		float y = mGhost.getScreenY();
		
		// AABB collision detection between the ghost and the edge of the screen
		// these numbers may have to be adjusted based on the device dpi
		
		if (x < -150) {
			return false;
		} 
		if (y < -150) {
			return false;
		} 
		if (x > 200) {
			return false;
		} 
		if (y > 300) {
			return false;
		}
		
		return true;
	}
	
	private class GameUpdate implements Runnable {
		
		private static final String TAG = "GameUpdate";

		private GameView mGameView;
		private Handler mHandler;
		
		public GameUpdate(GameView gameView, Handler handler)
		{
			if (Globals.DEBUG) Log.e(TAG, "GameUpdate()");
			mGameView = gameView;
			mHandler = handler;
		}
		
		@Override
		public void run() {		
			mHandler.postDelayed(this, 100);
			
			//if (Globals.DEBUG) Log.e(TAG, "run()");
			if (mHauntGhostFound) {
				Random rand = new Random();
				
				mGhost.update();
				mGhost.updateMatrix(getOrientation());
				
				// set the maximum transparency to 100 (roughly 40%)
				int staticAlpha = 100;
				// reduce the transparency by the distance between the centre of the screen and the ghost.
				staticAlpha -= (Math.abs(mGhost.getScreenX()) + Math.abs(mGhost.getScreenY())) / 10;
				// transparency must be >= 0
				if (staticAlpha < 0)
					staticAlpha = 0;
				
				mStatic.setAlpha(staticAlpha);
				
				if (ghostInView()) {
					for (int i = 0; i < mEnergyMeters.length; ++i) {
						mEnergyMeters[i].setEnergy(12 + rand.nextInt(12));
					}
				} else {
					for (int i = 0; i < mEnergyMeters.length; ++i) {
						mEnergyMeters[i].setEnergy(0 + rand.nextInt(7));
					}
				}
			} else {
				for (int i = 0; i < mEnergyMeters.length; ++i) {
					mEnergyMeters[i].setEnergy(0);
				}
				mStatic.setAlpha(0);
				mGhost.setAlpha(0);
			}
			
			mGameView.invalidate();
		}
	}

	private class ProximityIntentReceiver extends BroadcastReceiver {		
		@Override
		public void onReceive(Context context, Intent intent) {
			String key = LocationManager.KEY_PROXIMITY_ENTERING;
			Boolean entering = intent.getBooleanExtra(key, false);
			if (entering) {
				Log.v(TAG + ".ProximityIntentReceiver", "entering");
				mHauntGhostFound = true;
				Toast.makeText(context, "In range of the ghost.", Toast.LENGTH_SHORT).show();
			} else {
				Log.v(TAG + ".ProximityIntentReceiver", "exiting");
				Toast.makeText(context, "Too far from the ghost.", Toast.LENGTH_SHORT).show();
				mHauntGhostFound = false;
			}
		}
	}
	
	private void addProximityAlert() {
		
		SharedPreferences privatePreferences = getSharedPreferences("currentIndex",
				Context.MODE_PRIVATE);
		
		boolean hauntOn = privatePreferences.getBoolean("hauntTargetSearching", false);
		
		if (hauntOn) {
			double latitude = (double)privatePreferences.getFloat("hauntTargetLat", 0.0f);
			double longitude = (double)privatePreferences.getFloat("hauntTargetLng", 0.0f);
			
            mProximityIntentReceiver = new ProximityIntentReceiver();
            
            IntentFilter filter = new IntentFilter(PROX_ALERT_INTENT);
            registerReceiver(mProximityIntentReceiver, filter);
			
			Intent intent = new Intent(PROX_ALERT_INTENT);
            PendingIntent proximityIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            mLocationManager.addProximityAlert(
                   latitude, // the latitude of the central point of the alert region
                   longitude, // the longitude of the central point of the alert region
                   POINT_RADIUS, // the radius of the central point of the alert region, in meters
                   PROX_ALERT_EXPIRATION, // time for this proximity alert, in milliseconds, or -1 to indicate no                           expiration
                   proximityIntent // will be used to generate an Intent to fire when entry to or exit from the alert region is detected
            );
            Log.v(TAG, "Added proximity alert at: lat = " + latitude + ", lng = " + longitude);
		} else {
			Log.v(TAG, "haunt mode is off");
			mHauntGhostFound = true;
		}
	}
}
