package com.uklooneyjr.paracam.activities;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;

import com.example.paracam.R;
import com.uklooneyjr.paracam.Globals;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class GhostsActivity extends Activity {
	
	private static final String TAG = "GhostsActivity";
	
	private static final int PICK_IMAGE_REQ_CODE = 1;
	
	private Button mDownloadButton = null;
	private Button mUploadButton = null;
	
	private TextView mMessage = null;
	
	private File getDir() {
		File sdDir = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		return new File(sdDir, Globals.PICTURE_DIRECTORY + File.separator + "ghost_images");
	}
	
	private List<String> getList(File parentDir) {

	    ArrayList<String> inFiles = new ArrayList<String>();
	    String[] fileNames = parentDir.list();

	    for (String fileName : fileNames) {
	        if (fileName.toLowerCase(getResources().getConfiguration().locale).endsWith(".png")) {
	            inFiles.add(fileName);
	        }
	    }

	    return inFiles;
	}
	
	protected boolean inList(String string, List<String> list) {
		for (int i = 0; i < list.size(); ++i) {
			if (string.equals(list.get(i))) {
				return true;
			}
		}
		return false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.ghosts_activity_layout); 
		mDownloadButton = (Button)findViewById(R.id.ghosts_downloadBtn);
		mDownloadButton.setOnClickListener(new DownloadButtonClickListener());
		
		mUploadButton = (Button)findViewById(R.id.ghosts_uploadBtn);
		mUploadButton.setOnClickListener(new UploadButtonClickListener());
		
		mMessage = (TextView)findViewById(R.id.ghosts_message);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == PICK_IMAGE_REQ_CODE) {
				if (data != null) {
				    Uri selectedImageUri = data.getData();
				    String selectedImagePath = getPath(selectedImageUri);
				    Bitmap photo = getPreview(selectedImagePath);
				    new UploadBitmapTask().execute(photo);
				}
			}
		}
	}


	public String getPath(Uri uri) {
		String res = null;
	    String[] proj = { MediaStore.Images.Media.DATA };
	    Cursor cursor = getContentResolver().query(uri, proj, null, null, null);
	    if(cursor.moveToFirst()){
	       int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	       res = cursor.getString(column_index);
	    }
	    cursor.close();
	    return res;
	}

	public Bitmap getPreview(String fileName) {
	    File image = new File(fileName);

	    BitmapFactory.Options bounds = new BitmapFactory.Options();
	    bounds.inJustDecodeBounds = true;
	    BitmapFactory.decodeFile(image.getPath(), bounds);
	    if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) {
	        return null;
	    }
	    int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight
	        : bounds.outWidth;
	    BitmapFactory.Options opts = new BitmapFactory.Options();
	    opts.inSampleSize = originalSize / 100;
	    return BitmapFactory.decodeFile(image.getPath(), opts);
	}
	
	private void uploadBitmap(Bitmap bitmap) {
		HttpURLConnection connection = null;
		DataOutputStream outputStream = null;
		String urlServer = "http://mayar.abertay.ac.uk/~1105555/upload_image.php";
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary =  "*****";
		 
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1*1024*1024;
		 
		try
		{
			//create a file to write bitmap data
			File f = File.createTempFile("user_ghost", ".png", getCacheDir());
			
			//Convert bitmap to byte array
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
			byte[] bitmapdata = bos.toByteArray();

			//write the bytes in file
			FileOutputStream fos = new FileOutputStream(f);
			fos.write(bitmapdata);
		    FileInputStream fileInputStream = new FileInputStream(f);
		 
		    URL url = new URL(urlServer);
		    connection = (HttpURLConnection) url.openConnection();
		 
		    // Allow Inputs &amp; Outputs.
		    connection.setDoInput(true);
		    connection.setDoOutput(true);
		    connection.setUseCaches(false);
		 
		    // Set HTTP method to POST.
		    connection.setRequestMethod("POST");
		 
		    connection.setRequestProperty("Connection", "Keep-Alive");
		    connection.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);
		 
		    outputStream = new DataOutputStream( connection.getOutputStream() );
		    outputStream.writeBytes(twoHyphens + boundary + lineEnd);
		    outputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + f.getAbsolutePath() +"\"" + lineEnd);
		    outputStream.writeBytes(lineEnd);
		 
		    bytesAvailable = fileInputStream.available();
		    bufferSize = Math.min(bytesAvailable, maxBufferSize);
		    buffer = new byte[bufferSize];
		 
		    // Read file
		    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
		 
		    while (bytesRead > 0)
		    {
		        outputStream.write(buffer, 0, bufferSize);
		        Log.d(TAG, "" + bytesRead);
		        bytesAvailable = fileInputStream.available();
		        bufferSize = Math.min(bytesAvailable, maxBufferSize);
		        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
		    }
		 
		    outputStream.writeBytes(lineEnd);
		    outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
		 
		    // Responses from the server (code and message)
		    int serverResponseCode = connection.getResponseCode();
		    String serverResponseMessage = connection.getResponseMessage();
		    
		    Log.i(TAG, "Response Code " + serverResponseCode + ". " + serverResponseMessage);
		 
		    fileInputStream.close();
		    fos.flush();
		    fos.close();
		    outputStream.flush();
		    outputStream.close();
		}
		catch (Exception ex)
		{
		    Log.e(TAG, "Error uploading image " + ex.toString());
		}
	}
	
	private class DownloadButtonClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			new GetServerFilenamesTask().execute();
		}
	}
	
	private class GetServerFilenamesTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			String result = "";
			BufferedReader reader = null;
			try {
				HttpClient httpClient = new DefaultHttpClient();
				HttpGet httpGet = new HttpGet("http://mayar.abertay.ac.uk/~1105555/get_filenames.php");

				HttpResponse response = httpClient.execute(httpGet);
				HttpEntity entity = response.getEntity();
				reader = new BufferedReader(new InputStreamReader(entity.getContent()));
				StringBuilder str = new StringBuilder();
				while ((result = reader.readLine()) != null) {
					str.append(result);
				}
				result = str.toString();
			} catch(Exception e) {
				Log.e(TAG, "Error in Http Connection " + e.toString());
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Log.v(TAG, "Result = " + result);
			String[] strings = result.split("\\?");
			
			List<String> sdFiles = getList(getDir()); 
			int filesDownloaded = 0;
			
			for (int i = 0; i < sdFiles.size(); ++i) {
				Log.v(TAG, "sd filename = " + sdFiles.get(i));
			}
			
			for (int i = 0; i < strings.length; ++i) {
				if (strings[i].startsWith(".")) {
					continue;
				}
				if (inList(strings[i], sdFiles)) {
					continue;
				}
				
				new DownloadFileTask().execute(strings[i]);
				filesDownloaded ++;
			}
			mMessage.setText("Downloaded " + filesDownloaded + " new ghost(s).");
		}
		
	}
	
	private class DownloadFileTask extends AsyncTask<String, Void, File> {

		@Override
		protected File doInBackground(String... params) {
			Log.i(TAG, "Downloading file \"" + params[0] + "\"");
			File file = new File(getDir() + File.separator + params[0]);
			URL url = null;
			try {
				url = new URL("http://mayar.abertay.ac.uk/~1105555/ghost_images/" + params[0]);
		        URLConnection ucon = url.openConnection();
		        InputStream is = ucon.getInputStream();
		        BufferedInputStream bis = new BufferedInputStream(is);
	
		        ByteArrayBuffer baf = new ByteArrayBuffer(50);
		        int current = 0;
		        while ((current = bis.read()) != -1) {
		           baf.append((byte) current);
		        }
	
		        /* Convert the Bytes read to a String. */
		        FileOutputStream fos = new FileOutputStream(file);
		        fos.write(baf.toByteArray());
		        fos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return file;
		}

		@Override
		protected void onPostExecute(File result) {
			super.onPostExecute(result);
			
			Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
			Uri uri = Uri.fromFile(result);
			intent.setData(uri);
			sendBroadcast(intent);
			
			Log.i(TAG, "Downloaded file \"" + result.getName() + "\"");
		}
		
	}
	
	private class UploadButtonClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (Environment.getExternalStorageState().equals("mounted")) {
			    Intent intent = new Intent();
			    intent.setType("image/*");
			    intent.setAction(Intent.ACTION_PICK);
			    startActivityForResult(
			        Intent.createChooser(
			            intent,
			            "Select Picture:"),
			            PICK_IMAGE_REQ_CODE);
			}
		}
		
	}
	
	private class UploadBitmapTask extends AsyncTask<Bitmap, Void, Void> {
		@Override
		protected Void doInBackground(Bitmap... params) {
			uploadBitmap(params[0]);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			mMessage.setText("Successfully uploaded image.");
		}
		
		
	}
	
}
