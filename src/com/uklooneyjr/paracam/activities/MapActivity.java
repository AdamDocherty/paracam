package com.uklooneyjr.paracam.activities;

import java.io.File;

import com.example.paracam.R;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import com.uklooneyjr.paracam.Globals;
import com.uklooneyjr.paracam.ParaCamDbAdapter;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

public class MapActivity extends Activity {

	private static final String TAG = "MapActivity";
	
	private ParaCamDbAdapter mDbHelper;
	
	private GoogleMap mMap;
	
	private File getDir() {
		File sdDir = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		return new File(sdDir, Globals.PICTURE_DIRECTORY);
	}	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);
        
        mDbHelper = new ParaCamDbAdapter(this);
        mDbHelper.open();

        // Get a handle to the Map Fragment
        mMap = ((MapFragment) getFragmentManager()
                .findFragmentById(R.id.map)).getMap();

        mMap.setMyLocationEnabled(true);
        
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
    }
    
    @Override
	protected void onResume() {
		super.onResume();
		
		int selectedRowId = -1;
		SharedPreferences indexPrefs = getSharedPreferences("currentImageRowId",
				MODE_PRIVATE);
		if (indexPrefs.contains("currentImageRowId")) {
			selectedRowId = indexPrefs.getInt("currentImageRowId", -1);
			Log.d(TAG, "indexPref containts rowId");
		}
		Log.d(TAG, "Got pref rowId, " + selectedRowId);
		
		Cursor allGhosts = mDbHelper.fetchAllGhostEntries();
		try {
	        if (allGhosts.getCount() > 0) {
		        do {
		        	int ghostRowId = allGhosts.getInt(allGhosts.getColumnIndexOrThrow(ParaCamDbAdapter.KEY_ROWID));
		        	String ghostFilePath = getDir().getPath() + "/" + allGhosts.getString(allGhosts.getColumnIndexOrThrow(ParaCamDbAdapter.KEY_FILENAME));
		        	
		        	File file = new File(ghostFilePath);
		        	if (!file.exists()) {
		        		Log.w(TAG, "The file " + ghostFilePath + " does not exist.");
		        		continue;
		        	}
		        	
		        	BitmapFactory.Options options = new BitmapFactory.Options();
		        	options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		        	Bitmap ghostImage = BitmapFactory.decodeFile(ghostFilePath, options);
		        	BitmapDescriptor ghostImageDescriptor = BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(ghostImage, 90, 160, true));
		        	
		        	String dateCaptured = allGhosts.getString(allGhosts.getColumnIndexOrThrow(ParaCamDbAdapter.KEY_DATETIME));
		        	String username = allGhosts.getString(allGhosts.getColumnIndexOrThrow(ParaCamDbAdapter.KEY_USERNAME));
		        	
		        	Log.d(TAG, "getting position for ghost " + ghostRowId);
		        	LatLng position = new LatLng(allGhosts.getFloat(allGhosts.getColumnIndexOrThrow(ParaCamDbAdapter.KEY_LATITUDE)), 
		        			allGhosts.getFloat(allGhosts.getColumnIndexOrThrow(ParaCamDbAdapter.KEY_LONGITUDE)));
		        	Log.d(TAG, "Adding marker");
		        	mMap.addMarker(new MarkerOptions()
		        			.title("Ghost")
		        			.snippet("Captured on the " + dateCaptured + " by " + username + ".")
		        			.position(position)
		        			.icon(ghostImageDescriptor));
		        	Log.d(TAG, "Moving camera");
		        	if (ghostRowId == selectedRowId) {
		        		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));
		        	}
		        } while (allGhosts.moveToNext());
	        } else {
	        	Log.w(TAG, "database empty");
	        }
		} finally {
			allGhosts.close();
		}
	}
}