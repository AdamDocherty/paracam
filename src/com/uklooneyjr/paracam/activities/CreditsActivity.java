package com.uklooneyjr.paracam.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.example.paracam.R;
import com.uklooneyjr.paracam.Globals;

public class CreditsActivity extends Activity {
	
	private static final String TAG = "CreditsActivity";
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (Globals.DEBUG) Log.e(TAG, "onCreate()");
        
        setContentView(R.layout.credits);
	}
}
