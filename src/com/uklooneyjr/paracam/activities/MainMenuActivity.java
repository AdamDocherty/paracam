package com.uklooneyjr.paracam.activities;

import java.util.Locale;

import com.buuuk.android.gallery.ImageGridGallery;
import com.example.paracam.R;
import com.uklooneyjr.paracam.Globals;
import com.uklooneyjr.paracam.SmsListener;

import android.os.Bundle;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

@SuppressWarnings("unused")
public class MainMenuActivity extends Activity {

	private static final String TAG = "MainMenuActivity";
	
	private SmsListener mSmsListener = null;
	
	private Button mStartGameButton;
	private Button mHauntButton;
	private Button mGalleryButton;
	private Button mOptionsMenuButton;
	private Button mHelpButton;
	private Button mCreditsButton;
	private Button mGhostsButton;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (Globals.DEBUG) Log.e(TAG, "onCreate()");
        
        Globals.appVersion = getString(R.string.app_name) + " " + getString(R.string.app_version);
        Globals.verSdk = android.os.Build.VERSION.SDK_INT;
        if (Globals.DEBUG) Log.e(TAG, "currentApiVersionSdk=" + Globals.verSdk);
        
        setContentView(R.layout.main_menu_activity_layout);
        
        // Find all the buttons from the layout
        mStartGameButton = (Button)findViewById(R.id.button_startGame);
        mHauntButton = (Button)findViewById(R.id.button_haunt);
        mGalleryButton = (Button)findViewById(R.id.button_gallery);
        mOptionsMenuButton = (Button)findViewById(R.id.button_options);
        mCreditsButton = (Button)findViewById(R.id.button_credits);     
        mHelpButton = (Button)findViewById(R.id.button_help);    
        mGhostsButton = (Button)findViewById(R.id.button_ghosts);

                
        mStartGameButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
		        Intent intent = new Intent(MainMenuActivity.this, GameActivity.class);
		        startActivity(intent);
			}
        });
        
        mHauntButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
		        Intent intent = new Intent(MainMenuActivity.this, HauntActivity.class);
		        startActivity(intent);
			}
        });
        
        mGalleryButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MainMenuActivity.this, ImageGridGallery.class);
		        startActivity(intent);
			}
        });
        
        mGhostsButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
		        Intent intent = new Intent(MainMenuActivity.this, GhostsActivity.class);
		        startActivity(intent);
			}
        });
        
        mOptionsMenuButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MainMenuActivity.this, OptionsActivity.class);
		        startActivity(intent);
			}
        	
        });
        
        mHelpButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MainMenuActivity.this, HelpActivity.class);
		        startActivity(intent);
			}
        	
        });
        
        mCreditsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MainMenuActivity.this, CreditsActivity.class);
		        startActivity(intent);
			}
        	
        });

        mSmsListener = new SmsListener(this);
        
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
        
        registerReceiver(mSmsListener, filter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
    public void onDestroy() {
    	Log.d(TAG, "onDestroy()");
    	super.onDestroy();
    	unregisterReceiver(mSmsListener);
    }
	
	private void restartActivity() {
	    Intent intent = getIntent();
	    finish();
	    startActivity(intent);
	}
}
