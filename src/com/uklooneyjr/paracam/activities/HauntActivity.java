package com.uklooneyjr.paracam.activities;

import java.util.ArrayList;
import java.util.List;

import com.example.paracam.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class HauntActivity extends Activity {

	private static final String TAG = "HauntActivity";
	
	private static final int PICK_CONTACT = 0;
	
	private LocationManager mLocationManager;
	
	private EditText mPhoneNumberText;
	
	private Button mHauntButton = null; 
	private Button mContactPickerButton = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.haunt_activity_layout);
        
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        
        mPhoneNumberText = (EditText)findViewById(R.id.phoneNumber_editText);
        
        mHauntButton = (Button)findViewById(R.id.startHaunt_btn);
        mHauntButton.setOnClickListener(new StartHauntButtonClickListener());
        
        mContactPickerButton = (Button)findViewById(R.id.findFromContacts_btn);
        mContactPickerButton.setOnClickListener(new PickContactButtonClickListener());
	}
	
	private class StartHauntButtonClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			final String phoneNumber = PhoneNumberUtils.formatNumber(mPhoneNumberText.getText().toString());
			Log.i(TAG, "Phone number is " + phoneNumber);
			
			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						String message = "!ParaCam! I'm having a bit of ghost trouble, can you come help? " + getLatLngString();
						sendSMS(phoneNumber, message);
						finish();
						break;
					case DialogInterface.BUTTON_NEGATIVE:
						break;
					}
				}
			};
			
			AlertDialog.Builder builder = new AlertDialog.Builder(HauntActivity.this);
			builder.setMessage("Are you sure you want to haunt " + phoneNumber + ". They will need the app installed to work.")
				   .setPositiveButton("Yes", dialogClickListener)
				   .setNegativeButton("No", dialogClickListener)
				   .show();
			
		}
		
		private void sendSMS(String phoneNumber, String message) {
			Log.v(TAG, "phoneNumber = " + phoneNumber);
			Log.v(TAG, "message = " + message);
			
			SmsManager smsManager = SmsManager.getDefault();
			smsManager.sendTextMessage(phoneNumber, null, message, null, null);	
		}
		
		private String getLatLngString() {
			Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		    Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

		    long GPSLocationTime = 0;
		    if (null != locationGPS) { GPSLocationTime = locationGPS.getTime(); }

		    long NetLocationTime = 0;
		    if (null != locationNet) {
		        NetLocationTime = locationNet.getTime();
		    }

		    if ( 0 < GPSLocationTime - NetLocationTime ) {
		        return locationGPS.getLatitude() + " " + locationGPS.getLongitude();
		    }
		    else {
		    	return locationNet.getLatitude() + " " + locationNet.getLongitude();
		    }
		}
	}
	
	private class PickContactButtonClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			try {
				Intent intent = new Intent(Intent.ACTION_PICK, Contacts.CONTENT_URI);
		        startActivityForResult(intent, PICK_CONTACT);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case PICK_CONTACT:
				Cursor cursor = null;
				String phoneNumber = "";
				List<String> allNumbers = new ArrayList<String>();
				int phoneIndex = 0;
				try {
					Uri result = data.getData();
					String id  = result.getLastPathSegment();
					cursor = getContentResolver().query(Phone.CONTENT_URI, null, Phone.CONTACT_ID + "=?", new String[] { id }, null);
					phoneIndex = cursor.getColumnIndex(Phone.DATA);
					if (cursor.moveToFirst()) {
						while (cursor.isAfterLast() == false) {
							phoneNumber = cursor.getString(phoneIndex);
							allNumbers.add(phoneNumber);
							cursor.moveToNext();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (cursor != null) {
						cursor.close();
					}
					
					final CharSequence[] items = allNumbers.toArray(new String[allNumbers.size()]);
	                AlertDialog.Builder builder = new AlertDialog.Builder(HauntActivity.this);
	                builder.setTitle("Choose a number");
	                builder.setItems(items, new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface dialog, int item) {
	                        String selectedNumber = items[item].toString();
	                        selectedNumber = selectedNumber.replace("-", "");
	                        mPhoneNumberText.setText(selectedNumber);
	                    }
	                });
	                AlertDialog alert = builder.create();
	                if(allNumbers.size() > 1) {
	                    alert.show();
	                } else {
	                    String selectedNumber = phoneNumber.toString();
	                    selectedNumber = selectedNumber.replace("-", "");
	                    mPhoneNumberText.setText(selectedNumber);
	                }

	                if (phoneNumber.length() == 0) {  
	                    //no numbers found actions  
	                	AlertDialog.Builder noNumberAlertBuilder = new AlertDialog.Builder(HauntActivity.this);
	                	noNumberAlertBuilder.setTitle("No numbers available for this contact");
	                	noNumberAlertBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// do nothing
							}
	                	});
	                	noNumberAlertBuilder.show();
	                }  
				}
			}
		}
	}
	
}
