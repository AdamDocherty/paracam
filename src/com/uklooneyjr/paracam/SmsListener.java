package com.uklooneyjr.paracam;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

public class SmsListener extends BroadcastReceiver{

	private static final String TAG = "SmsListener";
	
    private SharedPreferences mPreferences;
    
    public SmsListener(Context context) {
    	mPreferences = context.getSharedPreferences("currentIndex",
				Context.MODE_PRIVATE);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        SmsMessage[] msgs = null;
        //String msg_from;
        
        if (bundle != null){
        	
            try{
                Object[] pdus = (Object[]) bundle.get("pdus");
                msgs = new SmsMessage[pdus.length];
                for(int i=0; i<msgs.length; i++){
                    msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                    //msg_from = msgs[i].getOriginatingAddress();
                    String msgBody = msgs[i].getMessageBody();
                    
                    parseMessage(msgBody);
                }
            } catch(Exception e) {
                        Log.d("Exception caught",e.getMessage());
            }
            
        }
    }
    
    private void parseMessage(String message) {
    	Log.v(TAG, "message = " + message);
    	String[] messageParts = message.split(" ");
    	if (messageParts[0].equals("!ParaCam!")) {
    		try {
		    	float lat = Float.parseFloat(messageParts[messageParts.length - 2]);
		    	float lng = Float.parseFloat(messageParts[messageParts.length - 1]);
		    	
		    	Log.v(TAG, "lat = " + lat + "\nlng = " + lng);
		    	
		    	SharedPreferences.Editor indexEditor = mPreferences.edit();
		    	indexEditor.putBoolean("hauntTargetSearching", true);
				indexEditor.putFloat("hauntTargetLat", lat);
				indexEditor.putFloat("hauntTargetLng", lng);
				indexEditor.commit();
    		} catch (NumberFormatException e) {
    			Log.e(TAG, "Attempted to parse message that was not the correct format, the message parts '" + messageParts[messageParts.length - 2] +
    					"' and '" + messageParts[messageParts.length - 1] + "' are incorrect");
    		}
    	} else {
    		String debugMessage = "'";
    		for (int i = 0; i < messageParts.length; ++i) {
    			debugMessage += messageParts[i] + "' '";
    		}
			Log.v(TAG, "debugMessage = " + debugMessage);
    	}
    }
}