package com.uklooneyjr.paracam;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.buuuk.android.util.FileUtils;
import com.example.paracam.R;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

public class Ghost extends Drawable {	
	private static final String TAG = "Ghost";
	
	private SharedPreferences mSharedPrefs;
	
	private static boolean sDebugPosition = false;
	private static final String DATA_FILE = "/imagelist.dat";
	
	private static final float TOP = 140.0f;
	private static final float BOTTOM = 20.0f;
	private static final float LEFT = -150.0f;		// stop the ghost from going directly south, the orientation 
	private static final float RIGHT = 150.0f;		//    sensors do not work correctly in these cases currently
	
	// Min and max velocity for easy difficulty
	private static final float MIN_VELOCITY_E = 0.1f;
	private static final float MAX_VELOCITY_E = 1.0f;
	// Min and max velocity for normal difficulty
	private static final float MIN_VELOCITY_N = 0.2f;
	private static final float MAX_VELOCITY_N = 1.5f;
	// Min and max velocity for hard difficulty
	private static final float MIN_VELOCITY_H = 0.3f;
	private static final float MAX_VELOCITY_H = 2.5f;
	// Alpha values for all difficulties
	private static final int GHOST_ALPHA_E = 192;
	private static final int GHOST_ALPHA_N = 128;
	private static final int GHOST_ALPHA_H = 96;
	
	private static final int NUMBER_GHOST_GRAPHICS = 3;		// not including the archie graphic
	
	private static final int DIFFICULTY_EASY = 1;
	private static final int DIFFICULTY_NORMAL = 2;
	private static final int DIFFICULTY_HARD = 3;
	
	private static final int GHOST_TYPE_ARCHIE = -1;
	private static final int GHOST_TYPE_JAP = 0;
	private static final int GHOST_TYPE_DOLL = 1;
	private static final int GHOST_TYPE_MUS = 2;
	
	private Handler mHandler;
	private Runnable mVelocitySwitchRunnable;
	
	private int mDifficulty;
	private int mGhostType;
	
	private float mXPos;				// x = 0 should be north
	private float mYPos;				// y = 0 should be directly under
	private float mVelocityX;
	private float mVelocityY;
	
	private float mScreenX;
	private float mScreenY;
	
	private Matrix mMatrix;
	private Bitmap mGraphic;
	private Paint  mPaint;
	
	private Resources mResources;
	static private List<String> sImageList;
	
	public Ghost(Resources res, SharedPreferences pref) {
		/**
		 * Ghost that will move around the screen.
		 * Will be drawn based on the orientation of the device
		 */
		
		if (Globals.DEBUG) Log.e(TAG, "Ghost()");
		
		mSharedPrefs = pref;
		mResources = res;
		mMatrix = new Matrix();
		mPaint = new Paint();
		
		// get necessary information from the preferences
		String difficulty = mSharedPrefs.getString("prefDifficulty", "2");
		mDifficulty = Integer.parseInt(difficulty);
		
		sDebugPosition = mSharedPrefs.getBoolean("prefDebugGhostPos", false);
		
		boolean archieMode = mSharedPrefs.getBoolean("prefArchieMode", false);	
		
		if (archieMode) { 
			mGhostType = GHOST_TYPE_ARCHIE;
		} else {
			Random rand = new Random();
			mGhostType = rand.nextInt(NUMBER_GHOST_GRAPHICS);
		}
		
		
		mHandler = new Handler();
		mVelocitySwitchRunnable = new Runnable() {
			@Override
			public void run() {
				if (mDifficulty > DIFFICULTY_EASY) {
					Random rand = new Random();
					int delay;
					switch (mDifficulty) {
					case DIFFICULTY_NORMAL:
						delay = 1000 + rand.nextInt(2000);
						break;
					case DIFFICULTY_HARD:
						delay = 500 + rand.nextInt(1000);
						break;
					default:
						delay = 3000;
					}
					
					mHandler.postDelayed(this, delay);
				}
			}
			
		};
		
		randomizePosition();
		randomizeVelocity();
		initaliseImageList();
		initialiseGraphic(res);
	}
	
	public void updateMatrix(float[] orientation) {
		float azimut = orientation[0];
		float pitch = orientation[1];
		
		mScreenX = (-azimut*360/(2*3.14159f) + mXPos) * 10.0f;
		mScreenY = (((float) Math.toDegrees(pitch)) - mYPos ) * 10.0f;
		mMatrix.setTranslate(mScreenX, mScreenY);
		mMatrix.postScale(4.0f, 4.0f);
	}
	
	public int getGhostType() {
		return mGhostType;
	}
	
	public float getX() {
		return mXPos;
	}
	
	public float getY() {
		return mYPos;
	}
	
	public float getScreenX() {
		return mScreenX;
	}
	
	public float getScreenY() {
		return mScreenY;
	}
	
	public void update() {
		move();
	}
	
	public void pause() {
		mHandler.removeCallbacks(mVelocitySwitchRunnable);
	}
	
	public void resume() {
		mHandler.postDelayed(mVelocitySwitchRunnable, 5000);
	}
	
	private void move() {
		mXPos += mVelocityX;
		mYPos += mVelocityY;
		
		// Keep to ghost from moving off the top and bottom of the screen
		if (mYPos > TOP) {
			mYPos = TOP;
			mVelocityY = -mVelocityY;
		}
		if (mYPos < BOTTOM) {
			mYPos = BOTTOM;
			mVelocityY = -mVelocityY;
		}
		// Stop the ghost from appearing directly south of this user, this can cause issues as the azimuth is sometimes inaccurate at this point
		if (mXPos > RIGHT) {
			mXPos = RIGHT;
			mVelocityX = -mVelocityX;
		}
		if (mXPos < LEFT) {
			mXPos = LEFT;
			mVelocityX = -mVelocityX;
		}
	}
	
	private void randomizeVelocity() {
		if(sDebugPosition) {
			mVelocityX = 0.0f;
			mVelocityY = 0.0f;
			return;
		}
		Random rand = new Random();
		
		switch (mDifficulty) {
		case DIFFICULTY_EASY:
			mVelocityX = rand.nextFloat() * (MAX_VELOCITY_E - MIN_VELOCITY_E) + MIN_VELOCITY_E;
			mVelocityY = rand.nextFloat() * (MAX_VELOCITY_E - MIN_VELOCITY_E) + MIN_VELOCITY_E;
			break;
		case DIFFICULTY_NORMAL:
			mVelocityX = rand.nextFloat() * (MAX_VELOCITY_N - MIN_VELOCITY_N) + MIN_VELOCITY_N;
			mVelocityY = rand.nextFloat() * (MAX_VELOCITY_N - MIN_VELOCITY_N) + MIN_VELOCITY_N;
			break;
		case DIFFICULTY_HARD:
			mVelocityX = rand.nextFloat() * (MAX_VELOCITY_H - MIN_VELOCITY_H) + MIN_VELOCITY_H;
			mVelocityY = rand.nextFloat() * (MAX_VELOCITY_H - MIN_VELOCITY_H) + MIN_VELOCITY_H;
			break;
		default:
			mVelocityX = rand.nextFloat() * (MAX_VELOCITY_N - MIN_VELOCITY_N) + MIN_VELOCITY_N;
			mVelocityY = rand.nextFloat() * (MAX_VELOCITY_N - MIN_VELOCITY_N) + MIN_VELOCITY_N;
		}
	}
	
	private void randomizePosition() {
		if(sDebugPosition) {
			mXPos = (RIGHT + LEFT) / 2;
			mYPos = (TOP + BOTTOM) / 2;
			return;
		}
		Random rand = new Random();
		
		mXPos = rand.nextFloat() * (RIGHT - LEFT) + LEFT;
		mYPos = rand.nextFloat() * (TOP - BOTTOM) + BOTTOM;	
	}
	
	private void initialiseGraphic(Resources res) {
			
		Random rand = new Random();
		mGhostType = rand.nextInt(sImageList.size());
		
		mGraphic = loadGraphicByType(mResources, mGhostType, true);
		mPaint.setAlpha(loadAlphaByDifficulty(mDifficulty));
		
	}
	
	private void initaliseImageList() {
		File data_directory = getDir();
		if (!data_directory.exists()) {
			if (data_directory.mkdir()) {
				FileUtils savedata = new FileUtils();
				/*Toast toast = Toast
						.makeText(
								ImageViewFlipper.this,
								"Please wait while we search your SD Card for images...",
								Toast.LENGTH_SHORT);
				toast.show();*/
				SystemClock.sleep(100);
				sImageList = FindFiles();
				savedata.saveArray(DATA_FILE, sImageList);

			} else {
				sImageList = FindFiles();
			}

		} else {
			File data_file = new File(DATA_FILE);
			if (!data_file.exists()) {
				FileUtils savedata = new FileUtils();
				/*Toast toast = Toast
						.makeText(
								ImageViewFlipper.this,
								"Please wait while we search your SD Card for images...",
								Toast.LENGTH_SHORT);
				toast.show();*/
				SystemClock.sleep(100);
				sImageList = FindFiles();
				savedata.saveArray(DATA_FILE, sImageList);
			} else {
				FileUtils readdata = new FileUtils();
				sImageList = readdata.loadArray(DATA_FILE);
			}
		}
	}
	
	private static File getDir() {
		File sdDir = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		return new File(sdDir, Globals.PICTURE_DIRECTORY + File.separator + "ghost_images");
	}
	
	public static Bitmap loadGraphicByType(Resources res, int type, boolean scaled) {
		/*switch (type) {
		case GHOST_TYPE_ARCHIE:
			return BitmapFactory.decodeResource(res, R.drawable.archie);
		case GHOST_TYPE_JAP:
			return BitmapFactory.decodeResource(res, R.drawable.fg_ghost_jap);
		case GHOST_TYPE_DOLL:
			return BitmapFactory.decodeResource(res, R.drawable.fg_ghost_doll);
		case GHOST_TYPE_MUS:
			return BitmapFactory.decodeResource(res, R.drawable.fg_ghost_mus);
		default:
			return BitmapFactory.decodeResource(res, R.drawable.archie);
		}*/

		if (type == GHOST_TYPE_ARCHIE) {
			return BitmapFactory.decodeResource(res, R.drawable.archie);
		}

		BitmapFactory.Options opts = new BitmapFactory.Options();
		if (scaled) {
			opts.inDensity = 2;
			opts.inTargetDensity = 5;
		}
		return BitmapFactory.decodeFile(sImageList.get(type), opts);
	}
	
	private List<String> FindFiles() {
		final List<String> tFileList = new ArrayList<String>();
		// array of valid image file extensions
		String[] imageTypes = mResources.getStringArray(R.array.image);
		FilenameFilter[] filter = new FilenameFilter[imageTypes.length];

		int i = 0;
		for (final String type : imageTypes) {
			filter[i] = new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith("." + type);
				}
			};
			i++;
		}

		FileUtils fileUtils = new FileUtils();
		File[] allMatchingFiles = fileUtils.listFilesAsArray(getDir(), filter,
				0);
		for (File f : allMatchingFiles) {
			tFileList.add(f.getAbsolutePath());
		}
		return tFileList;
	}
	
	public static int loadAlphaByDifficulty(int difficulty) {
		switch (difficulty) {
		case DIFFICULTY_EASY:
			return GHOST_ALPHA_E;
		case DIFFICULTY_NORMAL:
			return GHOST_ALPHA_N;
		case DIFFICULTY_HARD:
			return GHOST_ALPHA_H;
		default:
			return GHOST_ALPHA_N;
		}
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawBitmap(mGraphic, mMatrix, mPaint);
	}

	@Override
	public int getOpacity() {
		return 0;
	}

	@Override
	public void setAlpha(int alpha) {
	}

	@Override
	public void setColorFilter(ColorFilter cf) {
		// TODO Auto-generated method stub
		
	}
}
