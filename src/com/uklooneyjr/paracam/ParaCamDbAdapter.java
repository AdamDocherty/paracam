package com.uklooneyjr.paracam;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ParaCamDbAdapter {

	public static final String KEY_ROWID = "_id";
	public static final String KEY_LONGITUDE = "longitude";
	public static final String KEY_LATITUDE = "latitude";
	public static final String KEY_GHOST = "ghost";
	public static final String KEY_FILENAME = "filename";
	public static final String KEY_DATETIME = "datetime";
	public static final String KEY_USERNAME = "username";
	
	private static final String TAG = "ParaCamDbAdapter";
	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;
	
	private final Context mContext;
	
	private static final String DATABASE_NAME = "paracam_database";
	private static final String DATABASE_TABLE = "paracam_table";
    private static final int DATABASE_VERSION = 1;
	
	private static final String DATABASE_CREATE =
			"create table " + DATABASE_TABLE + " (" + KEY_ROWID + " integer primary key autoincrement" +
					", " + KEY_FILENAME + " text not null" +
					", " + KEY_GHOST + " integer not null" +
					", " + KEY_LONGITUDE + " float not null" +
					", " + KEY_LATITUDE + " float not null" +
					", " + KEY_DATETIME + " text not null" +
					", " + KEY_USERNAME + " text not null" +
					");";
	
	private static class DatabaseHelper extends SQLiteOpenHelper {
		
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			Log.i(TAG, "DatabaseHelper.onCreate()");
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			
		}
	}
	
	public ParaCamDbAdapter(Context context) {
		this.mContext = context;
	}
	
	public ParaCamDbAdapter open() throws SQLException {
		mDbHelper = new DatabaseHelper(mContext);
		mDb = mDbHelper.getWritableDatabase();
		return this;
	}
	
	public void close() {
		mDbHelper.close();
	}
	
	public long createGhostEntry(String filename, int ghost_id, float longitude, float latitude, String date, String username) {
		Log.i(TAG, "createGhostEntry()");
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_FILENAME, filename);
		initialValues.put(KEY_GHOST, ghost_id);
		initialValues.put(KEY_LONGITUDE, longitude);
		initialValues.put(KEY_LATITUDE, latitude);
		initialValues.put(KEY_DATETIME, date);
		initialValues.put(KEY_USERNAME, username);
		return mDb.insert(DATABASE_TABLE, null, initialValues);
	}
	
	public boolean deleteGhostEntry(long rowId) {
		return mDb.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
	}
	
	public Cursor fetchGhostEntry(long rowId) throws SQLException {
		Cursor cursor = mDb.query(true, DATABASE_TABLE, new String[] 
				{KEY_FILENAME, KEY_GHOST, KEY_LONGITUDE, KEY_LATITUDE, KEY_DATETIME, KEY_USERNAME},
				KEY_ROWID + "=" + rowId, null, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
		}
		return cursor;
	}
	
	public Cursor fetchGhostEntry(String filename) throws SQLException {
		Cursor cursor = mDb.query(true, DATABASE_TABLE, new String[] 
				{KEY_ROWID, KEY_GHOST, KEY_LONGITUDE, KEY_LATITUDE, KEY_DATETIME, KEY_USERNAME},
				KEY_FILENAME + "=" + "'" +  filename + "'", null, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
		}
		return cursor;
	}
	
	public Cursor fetchAllGhostEntries() {
		Cursor cursor = mDb.query(DATABASE_TABLE, null, null, null, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
		}
		return cursor;
	}
	
}
