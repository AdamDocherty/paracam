package com.uklooneyjr.paracam;

public class Globals {
	public static final boolean DEBUG = true;
	
	public static final String PICTURE_DIRECTORY = "ParaCam";
	
	public static String appVersion = "";
	public static int verSdk = 8;
}
