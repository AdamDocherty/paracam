package com.uklooneyjr.paracam;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

public class EnergyMeter extends Drawable {
	/**
	 * A series of dots that will light up based on the average energy.
	 * Energy must be passed in, the last 20 values passed in will be used to calculate the average
	 */
	
	private static final int HISTORICAL_ENERGY = 10;
	private static final int MAX_ENERGY = 20;
	private static final int X_STEP = 40;
	
	private Paint mOnPaint;
	private Paint mOffPaint;
	
	private int mXPos;
	private int mYPos;
	
	private int mHistoricalEnergyIndex;
	private int[] mHistoricalEnergy;
	
	public EnergyMeter(float x, float y) {
		mXPos = (int) x;
		mYPos = (int) y;
		
		mHistoricalEnergy = new int[HISTORICAL_ENERGY];
		
		mOnPaint = new Paint();
		mOnPaint.setARGB(255, 0, 255, 0); // green
		
		mOffPaint = new Paint();
		mOffPaint.setARGB(128, 64, 0, 0); // dark red, semi-transparent
	}
	
	@Override
	public void draw(Canvas canvas) {
		int energy = getAverageEnergy();
		
		// Draw the on dots
		for (int i = 0; i < energy; ++i) {
			canvas.drawCircle(mXPos + (i*X_STEP), mYPos, 10, mOnPaint);
		}
		// Draw the rest of the dots as off
		for (int i = energy; i < MAX_ENERGY; ++i) {
			canvas.drawCircle(mXPos + (i*X_STEP), mYPos, 10, mOffPaint);
		}
		

	}

	@Override
	public int getOpacity() {
		return 0;
	}

	@Override
	public void setAlpha(int alpha) {
	}

	@Override
	public void setColorFilter(ColorFilter cf) {
	}
	
	public void setEnergy(int energy) {
		if (energy > MAX_ENERGY) {
			energy = MAX_ENERGY;
		}
		
		if (mHistoricalEnergyIndex == -1) {
			// initialise the HistoricalEnergy array with the passed in value
			for (int i = 0; i < HISTORICAL_ENERGY; ++i) {
				mHistoricalEnergy[i] = energy;
				mHistoricalEnergyIndex = 0;
			}
		} else {
			// replace the oldest value in the array with the new value
			mHistoricalEnergy[mHistoricalEnergyIndex] = energy;
			if (++mHistoricalEnergyIndex >= HISTORICAL_ENERGY) {
				mHistoricalEnergyIndex = 0;
			}
		}
	}
	
	private int getAverageEnergy() {
		int avg = 0;
		for (int i = 0; i < HISTORICAL_ENERGY; ++i) {
			avg += mHistoricalEnergy[i];
		}
		avg /= HISTORICAL_ENERGY;
		
		return avg;
	}

}
