package com.uklooneyjr.paracam;

import java.util.Random;

import com.example.paracam.R;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;

public class Static extends Drawable {
	private static final int STATIC_VARIANCE = 100;
	
	private Bitmap[] mStaticGraphic;
	private Paint mPaint;
	
	public Static(Resources res) {
		mStaticGraphic = new Bitmap[3];
		// load the static bitmaps in
		mStaticGraphic[0] = BitmapFactory.decodeResource(res, R.drawable.fg_static_a1);
		mStaticGraphic[1] = BitmapFactory.decodeResource(res, R.drawable.fg_static_a2);
		mStaticGraphic[2] = BitmapFactory.decodeResource(res, R.drawable.fg_static_a3);
		
		mPaint = new Paint();
		mPaint.setAlpha(100);
	}

	@Override
	public void draw(Canvas canvas) {
		Random random = new Random();
		
		canvas.save();
		
		canvas.scale(10, 10);
		canvas.translate(-STATIC_VARIANCE, -STATIC_VARIANCE);
		int x = random.nextInt(STATIC_VARIANCE);
		int y = random.nextInt(STATIC_VARIANCE);
		canvas.drawBitmap(mStaticGraphic[0], x, y, mPaint);
		x = random.nextInt(STATIC_VARIANCE);
		y = random.nextInt(STATIC_VARIANCE);
		canvas.drawBitmap(mStaticGraphic[1], x, y, mPaint);
		x = random.nextInt(STATIC_VARIANCE);
		y = random.nextInt(STATIC_VARIANCE);
		canvas.drawBitmap(mStaticGraphic[2], x, y, mPaint);
		
		canvas.restore();
	}

	@Override
	public int getOpacity() {
		return PixelFormat.TRANSPARENT;
	}

	@Override
	public void setAlpha(int alpha) {
		mPaint.setAlpha(alpha);
	}

	@Override
	public void setColorFilter(ColorFilter filter) {
		mPaint.setColorFilter(filter);		
	}
}
