ParaCam
-------

ParaCam is an Android application for hunting ghosts using the phone's camera and accelerometer.
There are large variety of ghosts to catch, pictures of the ghosts are saved on the SD card and can be shared.
Location Data for where the ghosts are captured is also stored and can be viewd using the Google Maps API.

User Guide
==========
###Start Game
The main game. 
As you look around the room you will see static, the heavier the static the closer you are to finding the ghost.

![Static](http://i.imgur.com/befrddk.jpg?1)
![Ghost Found](http://i.imgur.com/xVS89tN.png?1)

Once the ghost is on screen, the readings at the bottom of the screen will increase, holding down the screen now will take a picture of the ghost and save it to the gallery (whilst removing any static).

###Gallery
View all the images that have been captured, first layout is the grid view where you can select and image.

![Grid View](http://i.imgur.com/NNn6HKk.png?1)

Second layout is a scrolling view where there are also buttons for deleting images, sharing them or locating them in the map.

![Scrolling View](http://i.imgur.com/iIp9IGN.png?1)

The map uses the Google Maps API and shows all ghosts that have ever been captured and are still saved, as well as the date they were captured and the person who caught them.

![Map View](http://i.imgur.com/HPYWUCF.jpg?1)

###Options
Several settings for the app such as debug settings, choosing your name, and difficulty settings.

###Help
Display instructions on how to play the game.

###Credits
Credits screen.